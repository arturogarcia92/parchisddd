package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.dice.FixedValueDice;
import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import com.adevgar.ddd.parchis.domain.token.Token;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GameTests {

    private final Dice normalDice = new Dice();

    private static int winsByPlayer1 = 0;
    private static int winsByPlayer2 = 0;
    private static int winsByPlayer3 = 0;
    private static int winsByPlayer4 = 0;

    //TODO -> refactor de gamesByPlayer
    @Test
    public void playNGames() {

        int numberOfMatches = 50000;
        long startTime = System.currentTimeMillis();
        IntStream.range(0, numberOfMatches).forEach(x -> playGame());


        System.out.println("Number of games played: "+numberOfMatches);
        System.out.println("Wons by player: ");
        System.out.println("Player1 "+winsByPlayer1);
        System.out.println("Player2 "+winsByPlayer2);
        System.out.println("Player3 "+winsByPlayer3);
        System.out.println("Player4 "+winsByPlayer4);
        long endTime = System.currentTimeMillis();
        double totalTime = startTime - endTime;
        System.out.println("Total time in seconds: "+(totalTime)*(-1)/(1000));
    }

    private void playGame() {
        int winner = new Game(4, normalDice).playGame();
        gamesByPlayer(winner+1);
    }

    private void gamesByPlayer(int winner){
        if (winner == 1){
            winsByPlayer1++;
        }
        if (winner == 2){
            winsByPlayer2++;
        }
        if (winner == 3){
            winsByPlayer3++;
        }
        if (winner == 4){
            winsByPlayer4++;
        }
    }





}
