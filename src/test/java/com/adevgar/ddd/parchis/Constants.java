package com.adevgar.ddd.parchis;

public class Constants {
    public static final int HOME_PLAYER_1 = 5;
    public static final int HOME_PLAYER_2 = 22;
    public static final int HOME_PLAYER_3 = 39;
    public static final int HOME_PLAYER_4 = 56;
    public static final int PLAYER_1 = 0;
    public static final int PLAYER_2 = 1;
    public static final int PLAYER_3 = 2;
    public static final int PLAYER_4 = 3;
}
