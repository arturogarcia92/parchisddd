package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.board.squares.Square;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import com.adevgar.ddd.parchis.domain.token.Token;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpawnTests {

    private final BoardSquares boardSquares = new BoardSquares(4);
    private final Players players = new Players(4);
    private final Movements movements = new Movements(boardSquares);


    @Test
    public void givenPlayer1With4TokensAtHomePositionWhenHeDoesNotThrow5ThenHeWontSpawn() {

        movements.movePlayerAndSteps(player1(), 6);
        movements.movePlayerAndSteps(player1(), 4);
        movements.movePlayerAndSteps(player1(), 3);
        movements.movePlayerAndSteps(player1(), 2);
        movements.movePlayerAndSteps(player1(), 1);

        Assert.assertEquals(0, retrieveTokensBySquarePositionInBoardSquares(Constants.HOME_PLAYER_1).size());
    }

    @Test
    public void givenPlayer1With2TokensSpawnedAtHomePositionWhenThrow5TwoMoreTimesThenOneTokenWillMove5positionsAndAnotherTokenWillBeSpawned() {
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player1());

        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(10).size());
        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(Constants.HOME_PLAYER_1).size());
    }

    @Test
    public void givenPlayer2With2TokensSpawnedAtHomePositionWhenThrow5TwoMoreTimesThenOneTokenWillMove5positionsAndAnotherTokenWillBeSpawned() {
        movements.spawnTokenToPlayer(player2());
        movements.spawnTokenToPlayer(player2());
        movements.spawnTokenToPlayer(player2());
        movements.spawnTokenToPlayer(player2());

        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(27).size());
        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(Constants.HOME_PLAYER_2).size());
    }

    @Test
    public void givenPlayer3With2TokensSpawnedAtHomePositionWhenThrow5TwoMoreTimesThenOneTokenWillMove5positionsAndAnotherTokenWillBeSpawned() {
        movements.spawnTokenToPlayer(player3());
        movements.spawnTokenToPlayer(player3());
        movements.spawnTokenToPlayer(player3());
        movements.spawnTokenToPlayer(player3());

        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(44).size());
        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(Constants.HOME_PLAYER_3).size());
    }

    @Test
    public void givenPlayer4With2TokensSpawnedAtHomePositionWhenThrow5TwoMoreTimesThenOneTokenWillMove5positionsAndAnotherTokenWillBeSpawned() {
        movements.spawnTokenToPlayer(player4());
        movements.spawnTokenToPlayer(player4());
        movements.spawnTokenToPlayer(player4());
        movements.spawnTokenToPlayer(player4());

        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(61).size());
        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(Constants.HOME_PLAYER_4).size());
    }

    @Test
    public void given2TokensInvadingPlayer2HomePositionWhenAnotherTokenTryToSpawnThenIsNotPossible(){
        movements.spawnTokenToPlayer(player1());
        movements.movePlayerAndSteps(player1(), 17);
        movements.spawnTokenToPlayer(player1());

        Token tokenToMoveInsideSquare22 = retrieveTokensBySquarePositionInBoardSquares(5).get(0);
        boardSquares.getSquareByPosition(22).addToken(tokenToMoveInsideSquare22);

        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(22).size());

        movements.spawnTokenToPlayer(player2());

        assertThatTokensInvadingPlayer2HomePositionBelongToPlayer1();
    }

    @Test
    public void givenTokenAtNormalPositionWhenAnotherTokenTryToEatThenHeCan() {
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player2());

        movements.movePlayerAndSteps(player1(), 20);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(25).size());
        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(22).size());

        movements.movePlayerAndSteps(player2(), 3);

        assertThatThereIsJust1TokenInPosition25BecausePlayer2AtePlayer1Token();
    }

    private void assertThatThereIsJust1TokenInPosition25BecausePlayer2AtePlayer1Token() {
        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(25).size());
        Assert.assertEquals(Constants.PLAYER_2, retrieveTokensBySquarePositionInBoardSquares(25).get(0).playerId());
    }


    @Test
    public void given4PlayersWhenTheyThrow5TwoTimesThenTheyWillHave2TokensInHome() {
        Spawn2TokensForEachPlayer();

        Square player1SpawnSquare = boardSquares.getSquareByPosition(Constants.HOME_PLAYER_1);
        Square player2SpawnSquare = boardSquares.getSquareByPosition(Constants.HOME_PLAYER_2);
        Square player3SpawnSquare = boardSquares.getSquareByPosition(Constants.HOME_PLAYER_3);
        Square player4SpawnSquare = boardSquares.getSquareByPosition(Constants.HOME_PLAYER_4);

        assertAllPlayersHave2TokensInHomeSquare(player1SpawnSquare, player2SpawnSquare, player3SpawnSquare, player4SpawnSquare);
    }


    private void assertAllPlayersHave2TokensInHomeSquare(Square player1SpawnSquare, Square player2SpawnSquare, Square player3SpawnSquare, Square player4SpawnSquare) {
        Assert.assertEquals(2, player1SpawnSquare.getTokensInside().size());
        Assert.assertEquals(2, player2SpawnSquare.getTokensInside().size());
        Assert.assertEquals(2, player3SpawnSquare.getTokensInside().size());
        Assert.assertEquals(2, player4SpawnSquare.getTokensInside().size());
    }

    private void Spawn2TokensForEachPlayer() {
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player2());
        movements.spawnTokenToPlayer(player2());
        movements.spawnTokenToPlayer(player3());
        movements.spawnTokenToPlayer(player3());
        movements.spawnTokenToPlayer(player4());
        movements.spawnTokenToPlayer(player4());
    }


    private void assertThatTokensInvadingPlayer2HomePositionBelongToPlayer1() {
        Assert.assertEquals(Constants.PLAYER_1, retrieveTokensBySquarePositionInBoardSquares(22).get(0).playerId());
        Assert.assertEquals(Constants.PLAYER_1, retrieveTokensBySquarePositionInBoardSquares(22).get(1).playerId());
    }

    private List<Token> retrieveTokensBySquarePositionInBoardSquares(int position) {
        return boardSquares.getSquareByPosition(position).getTokensInside();
    }


    private Player player1() {
        return players.getPlayerById(Constants.PLAYER_1);
    }

    private Player player2() {
        return players.getPlayerById(Constants.PLAYER_2);
    }

    private Player player3() {
        return players.getPlayerById(Constants.PLAYER_3);
    }

    private Player player4() {
        return players.getPlayerById(Constants.PLAYER_4);
    }

}
