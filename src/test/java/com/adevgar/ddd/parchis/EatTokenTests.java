package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import com.adevgar.ddd.parchis.domain.token.Token;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EatTokenTests {

    private final BoardSquares boardSquares = new BoardSquares(4);
    private final Players players = new Players(4);
    private final Movements movements = new Movements(boardSquares);


    @Test
    public void givenTokenFromPlayer2AtPosition23WhenPlayer1MoveToThisPositionThenHeWillEatTheToken() {
        movements.spawnTokenToPlayer(player2());
        movements.movePlayerAndSteps(player2(), 1);
        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(23).size());
        movements.spawnTokenToPlayer(player1());
        movements.movePlayerAndSteps(player1(), 18);
        Assert.assertEquals(1, retrieveTokensBySquarePositionInBoardSquares(23).size());

    }
    @Test
    public void givenTokenAtSafePositionWhenAnotherTokenOfDifferentPlayerTryToEatItThenHeCant() {
        movements.spawnTokenToPlayer(player1());
        movements.spawnTokenToPlayer(player2());

        movements.movePlayerAndSteps(player1(), 17);

        Assert.assertEquals(2, retrieveTokensBySquarePositionInBoardSquares(22).size());
    }

    @Test
    public void givenTwoTokensFormingAWallInNormalPositionsWhenAnotherPlayerTokenTryToEatThenHeCantAndItWontMove() {
        movements.spawnTokenToPlayer(player2());
        movements.movePlayerAndSteps(player2(), 1);
        movements.spawnTokenToPlayer(player2());

        Token tokenToMoveInsideAnotherSquare = boardSquares.getSquareByPosition(22).getTokensInside().get(0);
        boardSquares.getSquareByPosition(23).addToken(tokenToMoveInsideAnotherSquare);

        movements.spawnTokenToPlayer(player1());
        movements.movePlayerAndSteps(player1(), 18);

        assertThatTokenWontMoveToDestinationSquareBecause2TokensWereFormingAWall();
    }

    private void assertThatTokenWontMoveToDestinationSquareBecause2TokensWereFormingAWall() {
        Assert.assertEquals(2, boardSquares.getSquareByPosition(23).getTokensInside().size());
        Assert.assertEquals(1, boardSquares.getSquareByPosition(5).getTokensInside().size());
    }


    private List<Token> retrieveTokensBySquarePositionInBoardSquares(int position) {
        return boardSquares.getSquareByPosition(position).getTokensInside();
    }

    private List<Token> retrieveTokensBySquarePositionInLastPositions(int position, Player player) {
        return boardSquares.retrieveLastPositions().getSquareByPosition(position, player.id()).getTokensInside();
    }

    private Player player1() {
        return players.getPlayerById(Constants.PLAYER_1);
    }

    private Player player2() {
        return players.getPlayerById(Constants.PLAYER_2);
    }

    private Player player3() {
        return players.getPlayerById(Constants.PLAYER_3);
    }

    private Player player4() {
        return players.getPlayerById(Constants.PLAYER_4);
    }

}
