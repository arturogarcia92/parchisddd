package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.dice.FixedValueDice;
import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import com.adevgar.ddd.parchis.domain.token.Token;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BoardSquaresTests {

    private final BoardSquares boardSquares = new BoardSquares(4);
    private final Players players = new Players(4);
    private final Dice fixedDice = new Dice();

    @Test
    public void givenPlayer2FormingWallInSquare22WhenPlayer1TryToMoveHeWillStuckAt20() {
        spawnTokenToPlayer(Constants.PLAYER_1);
        spawnTokenToPlayer(Constants.PLAYER_1);
        spawnTokenToPlayer(Constants.PLAYER_2);
        spawnTokenToPlayer(Constants.PLAYER_2);

        movePlayerAndSteps(Constants.PLAYER_1, 15);
        movePlayerAndSteps(Constants.PLAYER_1, 15);
        movePlayerAndSteps(Constants.PLAYER_1, 15);
        movePlayerAndSteps(Constants.PLAYER_1, 15);
        movePlayerAndSteps(Constants.PLAYER_1, 15);

        Assert.assertEquals(2, boardSquares.getSquareByPosition(20).getTokensInside().size());

    }

    @Test
    public void givenPlayer1Throwing6ForThreeTimesThenOneRandomTokenWillDie(){
        spawnTokenToPlayer(Constants.PLAYER_1);
        movePlayerAndSteps(Constants.PLAYER_1, 6);



        Assert.assertTrue(player1().playerTokens().getTokens().stream().allMatch(Token::isAtBase));
    }

    private void movePlayerAndSteps(int playerId, int steps) {
        fixedDice.changeRandomizableDice(new FixedValueDice(steps));
        CurrentTurn turn = new CurrentTurn(boardSquares, players.getPlayerById(playerId), fixedDice);
        turn.processTurn();
    }

    private void spawnTokenToPlayer(int playerId) {
        fixedDice.changeRandomizableDice(new FixedValueDice(5));
        CurrentTurn turn = new CurrentTurn(boardSquares, players.getPlayerById(playerId), fixedDice);
        turn.processTurn();
    }

    private Player player1() {
        return players.getPlayerById(Constants.PLAYER_1);
    }

    private Player player2() {
        return players.getPlayerById(Constants.PLAYER_2);
    }

    private Player player3() {
        return players.getPlayerById(Constants.PLAYER_3);
    }

    private Player player4() {
        return players.getPlayerById(Constants.PLAYER_4);
    }



}
