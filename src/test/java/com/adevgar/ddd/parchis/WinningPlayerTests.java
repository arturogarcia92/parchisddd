package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WinningPlayerTests {

    private final BoardSquares boardSquares = new BoardSquares(4);
    private final Players players = new Players(4);
    private final Movements movements = new Movements(boardSquares);

    @Test
    public void Given4TokenBelongingPlayer1WhenTheyMoveAllInsideLastPositionAndWinThenThePlayer1WillBeTheWinner() {
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());

        Assert.assertTrue(player1().playerTokens().isPlayerWinner());
    }

    @Test
    public void Given4TokenBelongingPlayer2WhenTheyMoveAllInsideLastPositionAndWinThenThePlayer2WillBeTheWinner() {
        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());

        Assert.assertTrue(player2().playerTokens().isPlayerWinner());
    }

    @Test
    public void Given4TokenBelongingPlayer3WhenTheyMoveAllInsideLastPositionAndWinThenThePlayer3WillBeTheWinner() {
        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());

        Assert.assertTrue(player3().playerTokens().isPlayerWinner());
    }

    @Test
    public void Given4TokenBelongingPlayer4WhenTheyMoveAllInsideLastPositionAndWinThenThePlayer4WillBeTheWinner() {
        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());

        Assert.assertTrue(player4().playerTokens().isPlayerWinner());
    }

    @Test
    public void Given4PlayersWhenAllTokensMovesInsideLastPositionsAllPlayersWillWin() {
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());
        spawnTokenAndMoveToFinalSquareAndWin(player1());

        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());
        spawnTokenAndMoveToFinalSquareAndWin(player2());

        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());
        spawnTokenAndMoveToFinalSquareAndWin(player3());

        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());
        spawnTokenAndMoveToFinalSquareAndWin(player4());

        Assert.assertTrue(player1().playerTokens().isPlayerWinner());
        Assert.assertTrue(player2().playerTokens().isPlayerWinner());
        Assert.assertTrue(player3().playerTokens().isPlayerWinner());
        Assert.assertTrue(player4().playerTokens().isPlayerWinner());
    }


    private void spawnTokenAndMoveToFinalSquareAndWin(Player player) {
        movements.spawnTokenToPlayer(player);
        movements.movePlayerAndSteps(player, 60);
        movements.movePlayerAndSteps(player, 11);
    }

    private Player player1(){
        return players.getPlayerById(Constants.PLAYER_1);
    }

    private Player player2(){
        return players.getPlayerById(Constants.PLAYER_2);
    }

    private Player player3(){
        return players.getPlayerById(Constants.PLAYER_3);
    }

    private Player player4(){
        return players.getPlayerById(Constants.PLAYER_4);
    }

}
