package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.dice.FixedValueDice;
import com.adevgar.ddd.parchis.domain.board.dice.Randomizable;
import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;

class Movements {


    private final BoardSquares boardSquares;
    private final Dice fixedDice = new Dice();

    Movements(BoardSquares boardSquares) {
        this.boardSquares = boardSquares;
    }

    void movePlayerAndSteps(Player player, int steps) {
        fixedDice.changeRandomizableDice(new FixedValueDice(steps));
        CurrentTurn turn = new CurrentTurn(boardSquares, player, fixedDice);
        turn.processTurn();
    }

    void spawnTokenToPlayer(Player player) {
        fixedDice.changeRandomizableDice(new FixedValueDice(5));
        CurrentTurn turn = new CurrentTurn(boardSquares, player, fixedDice);
        turn.processTurn();
    }

    void changeDice(Randomizable randomizable){
        fixedDice.changeRandomizableDice(randomizable);
    }
}
