package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.player.Players;
import com.adevgar.ddd.parchis.domain.token.Token;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LastPositionTests {

    private final BoardSquares boardSquares = new BoardSquares(4);
    private final Players players = new Players(4);
    private final Movements movements = new Movements(boardSquares);


    @Test
    public void givenPlayer1TokenInsideLastPositionThenTheTokenWillThrow6ThreeTimes() {
        movements.spawnTokenToPlayer(player1());
        movements.movePlayerAndSteps(player1(), 68);
        movements.movePlayerAndSteps(player1(), 6);
        movements.movePlayerAndSteps(player1(), 1);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(6, player1()).size());
    }

    @Test
    public void givenPlayer1TokenWhenMovesInsideLastPositionThenTheTokenWillBeInsideSquareInLastPositions() {
        movements.spawnTokenToPlayer(player1());
        movements.movePlayerAndSteps(player1(), 68);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(5, player1()).size());
    }

    @Test
    public void givenPlayer2TokenWhenMovesInsideLastPositionThenTheTokenWillBeInsideSquareInLastPositions() {
        movements.spawnTokenToPlayer(player2());
        movements.movePlayerAndSteps(player2(), 68);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(5, player2()).size());
    }

    @Test
    public void givenPlayer3TokenWhenMovesInsideLastPositionThenTheTokenWillBeInsideSquareInLastPositions() {
        movements.spawnTokenToPlayer(player3());
        movements.movePlayerAndSteps(player3(), 68);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(5, player3()).size());
    }

    @Test
    public void givenPlayer4TokenWhenMovesInsideLastPositionThenTheTokenWillBeInsideSquareInLastPositions() {
        movements.spawnTokenToPlayer(player4());
        movements.movePlayerAndSteps(player4(), 68);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(5, player4()).size());
    }

    @Test
    public void givenPlayer1TokenInNormalBoardSquareWhenMovingInsideLastPositionWithABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToPenultimateSquareBeforeLastPositions(player1());
        movements.movePlayerAndSteps(player1(), 22);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player1()).size());
    }

    @Test
    public void givenPlayer2TokenInNormalBoardSquareWhenMovingInsideLastPositionWithABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToPenultimateSquareBeforeLastPositions(player2());
        movements.movePlayerAndSteps(player2(), 22);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player2()).size());
    }

    @Test
    public void testwalllastpositions() {
        movements.spawnTokenToPlayer(player2());
        Token tokenToMoveInsideAnotherSquare = boardSquares.getSquareByPosition(22).getTokensInside().get(0);
        boardSquares.getSquareByPosition(22).removeToken(tokenToMoveInsideAnotherSquare);
        boardSquares.retrieveLastPositions().getSquareByPosition(2, player2().id()).addToken(tokenToMoveInsideAnotherSquare);
        tokenToMoveInsideAnotherSquare.moveTokenInNormalSteps(60);
        tokenToMoveInsideAnotherSquare.moveTokenInLastPositionSteps(2);
        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(2, player2()).size());

        movements.spawnTokenToPlayer(player2());
        tokenToMoveInsideAnotherSquare = boardSquares.getSquareByPosition(22).getTokensInside().get(0);
        boardSquares.getSquareByPosition(22).removeToken(tokenToMoveInsideAnotherSquare);
        boardSquares.retrieveLastPositions().getSquareByPosition(2, player2().id()).addToken(tokenToMoveInsideAnotherSquare);
        tokenToMoveInsideAnotherSquare.moveTokenInNormalSteps(60);
        tokenToMoveInsideAnotherSquare.moveTokenInLastPositionSteps(2);
        Assert.assertEquals(2, retrieveTokensBySquarePositionInLastPositions(2, player2()).size());

        movements.spawnTokenToPlayer(player2());
        tokenToMoveInsideAnotherSquare = boardSquares.getSquareByPosition(22).getTokensInside().get(0);
        boardSquares.getSquareByPosition(22).removeToken(tokenToMoveInsideAnotherSquare);
        boardSquares.getSquareByPosition(26).addToken(tokenToMoveInsideAnotherSquare);
        tokenToMoveInsideAnotherSquare.moveTokenInNormalSteps(4);
        Assert.assertEquals(1, boardSquares.getSquareByPosition(26).getTokensInside().size());

        movements.spawnTokenToPlayer(player2());
        tokenToMoveInsideAnotherSquare = boardSquares.getSquareByPosition(22).getTokensInside().get(0);
        boardSquares.getSquareByPosition(22).removeToken(tokenToMoveInsideAnotherSquare);
        boardSquares.getSquareByPosition(29).addToken(tokenToMoveInsideAnotherSquare);
        tokenToMoveInsideAnotherSquare.moveTokenInNormalSteps(7);
        Assert.assertEquals(1, boardSquares.getSquareByPosition(29).getTokensInside().size());

        movements.movePlayerAndSteps(player2(),5);

    }

    @Test
    public void givenPlayer3TokenInNormalBoardSquareWhenMovingInsideLastPositionWithABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToPenultimateSquareBeforeLastPositions(player3());
        movements.movePlayerAndSteps(player3(), 22);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player3()).size());
    }

    @Test
    public void givenPlayer4TokenInNormalBoardSquareWhenMovingInsideLastPositionWithABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToPenultimateSquareBeforeLastPositions(player4());
        movements.movePlayerAndSteps(player4(), 22);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player4()).size());
    }


    @Test
    public void givenPlayer1TokenInsideLastPositionWhenThrowABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToLastSquareBeforeWin(player1());
        movements.movePlayerAndSteps(player1(), 14);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player1()).size());
    }

    @Test
    public void givenPlayer2TokenInsideLastPositionWhenThrowABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToLastSquareBeforeWin(player2());
        movements.movePlayerAndSteps(player2(), 14);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player2()).size());
    }

    @Test
    public void givenPlayer3TokenInsideLastPositionWhenThrowABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToLastSquareBeforeWin(player3());
        movements.movePlayerAndSteps(player3(), 14);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player3()).size());
    }

    @Test
    public void givenPlayer4TokenInsideLastPositionWhenThrowABigThrownThenItWillBounces2Times() {
        spawnTokenAndMoveToLastSquareBeforeWin(player4());
        movements.movePlayerAndSteps(player4(), 14);

        Assert.assertEquals(1, retrieveTokensBySquarePositionInLastPositions(7, player4()).size());
    }

    private List<Token> retrieveTokensBySquarePositionInLastPositions(int position, Player player) {
        return boardSquares.retrieveLastPositions().getSquareByPosition(position, player.id()).getTokensInside();
    }

    private void spawnTokenAndMoveToPenultimateSquareBeforeLastPositions(Player player) {
        movements.spawnTokenToPlayer(player);
        movements.movePlayerAndSteps(player, 62);
    }

    private void spawnTokenAndMoveToLastSquareBeforeWin(Player player) {
        movements.spawnTokenToPlayer(player);
        movements.movePlayerAndSteps(player, 70);
    }

    private Player player1() {
        return players.getPlayerById(Constants.PLAYER_1);
    }

    private Player player2() {
        return players.getPlayerById(Constants.PLAYER_2);
    }

    private Player player3() {
        return players.getPlayerById(Constants.PLAYER_3);
    }

    private Player player4() {
        return players.getPlayerById(Constants.PLAYER_4);
    }

}
