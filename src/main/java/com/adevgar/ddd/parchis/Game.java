package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Players;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class Game {

    private final Players players;
    private final BoardSquares boardSquares = new BoardSquares(4);
    private final List<CurrentTurn> gameTurns = new ArrayList();
    private final Dice dice;
    final static Logger logger = Logger.getLogger(String.valueOf(Game.class));

    public Game(int numPlayers, Dice dice) {
        this.players = new Players(numPlayers);
        this.dice = dice;
    }

    public int playGame() {

        printBoardAndPlayersInfo();
        while (thereIsNoWinnerYet()) {
            createAndExecuteTurns();
        }
        return winnerPlayerId();
    }

    private boolean thereIsNoWinnerYet() {
        return players.players().stream().noneMatch(x -> x.playerTokens().isPlayerWinner());
    }

    private void createAndExecuteTurns() {
        createAndExecuteTurn();
    }

    private int winnerPlayerId() {
        //logger.info("THE WINNER IS PLAYER " + (getPlayerIdOfLastTurn() + 1));
        return getPlayerIdOfLastTurn();
    }

    private void createAndExecuteTurn() {
        CurrentTurn turn = new CurrentTurn(boardSquares, players.checkWhichPlayerHasToPlay(gameTurns.size()), dice);
        gameTurns.add(turn);
        //logger.info("****** TURN "+gameTurns.size()+" ******");
        turn.processTurn();
        players.checkWhichPlayerHasToPlay(gameTurns.size()-1).playerTokens().tokenPositions();
    }

    private int getPlayerIdOfLastTurn() {
        return gameTurns.get(gameTurns.size() - 1).getAssociatedPlayerToCurrentTurn().id();
    }


    private void printBoardAndPlayersInfo() {
        boardSquares.printSquares();
        players.printPlayersInfo();
    }

    private void printPlayerAndTokensInfo() {
        boardSquares.printCurrentStatus();
        boardSquares.retrieveLastPositions().printCurrentStatus();
    }


}
