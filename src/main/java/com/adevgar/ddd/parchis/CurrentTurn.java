package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.board.squares.Square;
import com.adevgar.ddd.parchis.domain.board.squares.Type;
import com.adevgar.ddd.parchis.domain.movements.*;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.token.PlayerTokens;
import com.adevgar.ddd.parchis.domain.token.Token;

import java.util.Optional;
import java.util.logging.Logger;

public class CurrentTurn {

    private Player currentPlayer;
    final static Logger logger = Logger.getLogger(String.valueOf(CurrentTurn.class));
    private final BoardSquares boardSquares;
    private final Dice dice;


    public CurrentTurn(BoardSquares boardSquares, Player currentPlayer, Dice dice) {
        this.boardSquares = boardSquares;
        this.currentPlayer = currentPlayer;
        this.dice = dice;
    }

    //TODO no me convence que esto vaya aquí, la otra opcion es añadir returns en Game del current player.
    public Player getAssociatedPlayerToCurrentTurn() {
        return currentPlayer;
    }


    private int throwDice() {
        return dice.rollDice();
    }

    public void processTurn() {
        int currentThrown = throwDice();
        //logger.info("------ Player " + (currentPlayer.id()+1) + " turn with thrown: " + currentThrown + " ------");
        generateAndDoMovements(currentThrown);
        if(isPossibleToPlayAnotherTurn(currentThrown)){
            processTurn();
        }
    }

    private boolean isPossibleToPlayAnotherTurn(int currentThrown) {
        return currentThrown == 6 && !dice.checkIfTripleSix();
    }

    private void generateAndDoMovements(int thrown) {
        evaluatePossibleMovements(thrown).doMove(currentPlayer, thrown);
    }

    private Movements evaluatePossibleMovements(int thrown) {
        Token movableToken = getRandomToken(thrown);


        //TODO refactor de esto
        if(dice.checkIfTripleSix()) {
            killToken();
            return new NoMovements();
        }

        if (isPossibleToSpawn(thrown, currentPlayer.playerTokens())) {
            //TODO esto deberia ir dentro de boardsquares como el getrandommovable
            Token tokenToSpawn = boardSquares.getRandomTokenAtHome(currentPlayer.playerTokens());
            return new SpawnMovements(boardSquares, tokenToSpawn);
        }

        if(noTokenToMove(movableToken)){
            return new NoMovements();
        }

        if (isPossibleToEat(movableToken, thrown)) {
            return new EatAndNormalMovements(boardSquares, movableToken);
        }

        if (isInsideLastMovements(movableToken, thrown)) {
            return new LastMovements(boardSquares, movableToken);
        }
        return new NormalMovements(boardSquares, movableToken);
    }

    private void killToken() {
        Optional<Token> deadToken = retrieveRandomTokenToDie();
        if (retrieveRandomTokenToDie().isPresent()){
            //logger.info("Player " +(currentPlayer.id()+1)+ " token (was at "+deadToken.get().retrievePosition()+" position) has died because he got 6 three times ");
            Square tokenSquare = boardSquares.getSquareByPosition(deadToken.get().retrievePosition());
            tokenSquare.removeToken(deadToken.get());
            deadToken.get().die();
        }
    }

    private Optional<Token> retrieveRandomTokenToDie() {
        return currentPlayer.playerTokens().getTokens().stream().filter(x -> x.movedFinalSteps() == 0 && !x.isAtBase()).findFirst();
    }

    private Token getRandomToken(int thrown) {
        Token movabletoken = getRandomMovableTokenFromBoardSquares(thrown);

        if (noTokenToMove(movabletoken)) {
            movabletoken = getRandomMovableTokenFromLastPositions(thrown);
        }

        return movabletoken;
    }


    private Token getRandomMovableTokenFromBoardSquares(int thrown) {
        return boardSquares.getRandomMovableToken(thrown, currentPlayer.playerTokens());
    }

    private Token getRandomMovableTokenFromLastPositions(int thrown) {
        return boardSquares.retrieveLastPositions().getRandomMovableToken(thrown, currentPlayer.playerTokens());
    }

    private boolean isInsideLastMovements(Token movableToken, int thrown) {
        if(movableToken != null) {
            return (movableToken.movedSteps() + thrown) > 63 || movableToken.retrievePositionInLastPositions() != 0;
        }
        return false;
    }


    private boolean noTokenToMove(Token movableToken) {
        return movableToken == null;
    }

    private boolean isPossibleToSpawn(int thrown, PlayerTokens playerTokens) {
        return thrown == 5 && playerTokens.areLeftTokensAtBase() && homeSquareHaveSpace(playerTokens);
    }

    private boolean homeSquareHaveSpace(PlayerTokens playerTokens) {
        Square homeSquare = boardSquares.getSquareByPosition(playerTokens.getTokens().get(0).playerHomePosition());
        return homeSquare.getTokensInside().size() < 2;
    }

    private boolean isPossibleToEat(Token movableToken, int thrown) {

        if (movableToken != null) {
            int destination = destinationPosition(movableToken, thrown);
            //todo revisar esta condicion-
            if (destination > 0 && movableToken.retrievePositionInLastPositions() == 0) {
                Square destinationSquare = boardSquares.getSquareByPosition(destinationPosition(movableToken, thrown));
                return destinationSquare.getTokensInside().size() != 0 &&
                        destinationSquare.getType() == Type.NORMAL &&
                        destinationSquare.getTokensInside().size() != 2;
            }
        }
        return false;
    }

    private int destinationPosition(Token movableToken, int thrown) {
        if (destinationIsNotInsideLastPositions(movableToken, thrown)) {
            return (movableToken.retrievePosition() + thrown)%68;
        } else return 0;
    }

    private boolean destinationIsNotInsideLastPositions(Token movableToken, int thrown) {
        return (movableToken.movedSteps()+thrown < 63);
    }


}
