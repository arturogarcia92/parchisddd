package com.adevgar.ddd.parchis.domain.movements;

import com.adevgar.ddd.parchis.domain.player.Player;

public interface Movements {

    void doMove(Player player, int thrown);
}
