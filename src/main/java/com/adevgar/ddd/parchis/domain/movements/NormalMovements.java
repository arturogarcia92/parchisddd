package com.adevgar.ddd.parchis.domain.movements;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.token.Token;

public class NormalMovements implements Movements {

    private final BoardSquares boardSquares;
    private final Token movableToken;

    public NormalMovements(BoardSquares boardSquares, Token movableToken) {
        this.boardSquares = boardSquares;
        this.movableToken = movableToken;
    }

    @Override
    public void doMove(Player player, int thrown) {
        moveRandomTokenToDestination(thrown);
    }

    private void moveRandomTokenToDestination(int thrown) {
        boardSquares.removeTokenFromOriginalSquare(movableToken);
        boardSquares.addTokenToDestinationSquareAndMove(movableToken, thrown);
    }

}
