package com.adevgar.ddd.parchis.domain.movements;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import com.adevgar.ddd.parchis.domain.board.squares.Square;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.token.Token;

import java.util.logging.Logger;

public class EatAndNormalMovements implements Movements {

    private final BoardSquares boardSquares;
    private final Token movableToken;
    final static Logger logger = Logger.getLogger(String.valueOf(EatAndNormalMovements.class));

    public EatAndNormalMovements(BoardSquares boardSquares, Token movableToken) {
        this.boardSquares = boardSquares;
        this.movableToken = movableToken;
    }

    @Override
    public void doMove(Player player, int thrown) {
        moveRandomTokenToDestination(thrown, player);
    }

    private void moveRandomTokenToDestination(int thrown, Player player) {
        boardSquares.removeTokenFromOriginalSquare(movableToken);
        boardSquares.addTokenToDestinationSquareAndMove(movableToken, thrown);
        eatToken(player, movableToken);
    }

    private void eatToken(Player player, Token movableToken) {
        Square currentSquare = boardSquares.getSquareByPosition(movableToken.retrievePosition());
        if (isAllowedToEat(currentSquare, player)) {
            Token tokenToDie = currentSquare.getTokensInside().get(0).die();
            //logger.info("The player "+(player.id()+1)+" has eaten a token from player "+(tokenToDie.playerId()+1)+ " at position "+movableToken.retrievePosition());
            removeTokenFromSquare(currentSquare, tokenToDie);
        }
    }

    private boolean isAllowedToEat(Square destinationSquare, Player player) {
        return !destinationSquare.getTokensInside().isEmpty() && !isTokenInDestinationBelongingToPlayer(destinationSquare, player);
    }

    private boolean isTokenInDestinationBelongingToPlayer(Square squareToMoveInside, Player player) {
        return player.id() == squareToMoveInside.getTokensInside().get(0).playerId();
    }

    private void removeTokenFromSquare(Square square, Token tokenToDie) {
        square.removeToken(tokenToDie);
    }

}


