package com.adevgar.ddd.parchis.domain.board.squares;

import com.adevgar.ddd.parchis.domain.token.Token;

import java.util.ArrayList;
import java.util.List;

public class Square {

    private final Type type;
    private final int position;
    private ArrayList tokensInside = new ArrayList();

    public Square(Type type, int position) {
        this.type = type;
        this.position = position;

    }

    public Type getType() {
        return type;
    }

    public void addToken(Token token){
        if(tokensInside.size()<=1) {
            tokensInside.add(token);
        }
        else throw new IllegalArgumentException("Just 2 tokens can be inside a square");
    }

    public void removeToken(Token token){
        tokensInside.remove(token);
    }

    public void removeToken(){
        tokensInside.clear();
    }

    public boolean wall(){
        if (this.tokensInside.size() == 2){
            return checkTokensInside();
        }

        return false;
    }

    public List<Token> getTokensInside(){
        return this.tokensInside;
    }


    private boolean checkTokensInside() {
        if (this.type == Type.SPECIAL || this.type == Type.HOME){
            return true;
        }

        return false;
    }


}


