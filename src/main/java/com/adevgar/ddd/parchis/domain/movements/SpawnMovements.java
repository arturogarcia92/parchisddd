package com.adevgar.ddd.parchis.domain.movements;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.board.squares.Square;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.token.Token;

public class SpawnMovements implements Movements {

    private final BoardSquares boardSquares;
    private final Token movableToken;

    public SpawnMovements(BoardSquares boardSquares, Token movableToken) {
        this.boardSquares = boardSquares;
        this.movableToken = movableToken;
    }


    //TODO -> no me gusta que este no use el thrown...
    @Override
    public void doMove(Player player, int thrown) {
        spawnTokenToHomePosition();
    }

    private void spawnTokenToHomePosition() {
        addTokenToSpawnSquare(movableToken);
    }

    private void addTokenToSpawnSquare(Token movableToken) {
        movableToken.spawnToken();
        int destinationSquareIndex = movableToken.retrievePosition();
        Square destinationSquare = boardSquares.getSquareByPosition(destinationSquareIndex);
        moveTokenInsideSquare(destinationSquare, movableToken);
    }

    private void moveTokenInsideSquare(Square squareToMoveInside, Token tokenToAdd) {
        squareToMoveInside.addToken(tokenToAdd);
    }

}
