package com.adevgar.ddd.parchis.domain.board.dice;

import java.util.ArrayList;
import java.util.List;

public final class Dice {


    private Randomizable randomizable;
    private final List<Integer> diceThrowsHistoric = new ArrayList();

    public Dice(){
        this.randomizable = new SixSidesRandomizableDice();
    }



    public void changeRandomizableDice(Randomizable randomizable) {
        this.randomizable = randomizable;
    }

    public int rollDice(){
        int roll = randomizable.random();
        diceThrowsHistoric.add(roll);
        return roll;
    }

    public boolean checkIfTripleSix(){
        if (diceThrowsHistoric.size() > 3) return checkLastThreeThrowns();
        return false;
    }

    private boolean checkLastThreeThrowns(){
        return getLastThreeThrownsSum() == 18;
    }

    private int getLastThreeThrownsSum(){
        int firstElement = diceThrowsHistoric.get(diceThrowsHistoric.size()-1);
        int secondElement = diceThrowsHistoric.get(diceThrowsHistoric.size()-2);
        int thirdElement = diceThrowsHistoric.get(diceThrowsHistoric.size()-3);

        return firstElement+secondElement+thirdElement;
    }


}
