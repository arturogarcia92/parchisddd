package com.adevgar.ddd.parchis.domain.board.squares;

import com.adevgar.ddd.parchis.domain.token.PlayerTokens;
import com.adevgar.ddd.parchis.domain.token.Token;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BoardSquares {

    private final List<Square> boardSquares = new ArrayList<>();
    private int size = 69;
    final static Logger logger = Logger.getLogger(String.valueOf(BoardSquares.class));
    //refactor de esto
    private int index = 1;
    private LastPositionsSquares lastPositionsSquares;

    public BoardSquares(int numPlayers) {
        createBoardSquares();
        createLastPositions(numPlayers);
    }

    private void createBoardSquares() {
        IntStream.range(1, size).forEach(this::createSquares);
    }

    private void createLastPositions(int numPlayers) {
        lastPositionsSquares = new LastPositionsSquares(numPlayers);
    }

    public boolean checkThereAreAWallInMoveRange(List<Square> squaresToCheck) {
        return squaresToCheck.stream().anyMatch(Square::wall);
    }

    public Token getRandomTokenAtHome(PlayerTokens playerTokens){
        return playerTokens.getTokens().stream().filter(Token::isAtBase).findFirst().get();
    }

    public LastPositionsSquares retrieveLastPositions() {
        return lastPositionsSquares;
    }

    public Token getRandomMovableToken(int thrown, PlayerTokens playerTokens) {
        return playerTokens.getTokens()
                .stream()
                .filter(token -> !checkObstacles(token, thrown))
                .findFirst()
                .orElse(null);
    }


    private boolean checkObstacles(Token token, int thrown) {
        int firstSquareToCheckObstacles = firstSquareToCheckObstacles(token);
        int possibleDestination = calculateDestinationMove(thrown, token.retrievePosition());

        Square lastSquare = getSquareByPosition(possibleDestination);

        List<Square> squaresToCheck = retrieveSquaresByIndex(squaresInsideMovement(firstSquareToCheckObstacles, possibleDestination));

        //todo no me mola esto de pasarle lastSquare, cuando pongamos logica de obstacles para conseguir el movable token en lastpositions, desaparecera esto
        return areObstaclesInTheSquaresBetweenMoveRange(token, squaresToCheck, lastSquare) || areObstaclesInsideLastPositionsComingFromBoardSquares(token, thrown);
    }

    private boolean areObstaclesInsideLastPositionsComingFromBoardSquares(Token token, int thrown) {
        if (token.movedSteps() + thrown > 63){
            int destinationInLastPositions = ((token.movedSteps() + thrown) % 63)%8;
            return lastPositionsSquares.getSquareByPosition(destinationInLastPositions, token.playerId()).getTokensInside().size() == 2;
        }
        return false;
    }


    //TODO mirar esto de la position + 1
    private int firstSquareToCheckObstacles(Token token) {
        return token.retrievePosition() + 1;
    }


    private List<Integer> squaresInsideMovement(int current, int destination) {
        List<Integer> moved = IntStream
                .range(current, destination + 1)
                .boxed()
                .collect(Collectors.toList());
        return squaresNumber(moved);
    }

    private List<Integer> squaresNumber(List<Integer> moved) {
        return moved.stream().map(x -> x % 68).collect(Collectors.toList());
    }

    private boolean areObstaclesInTheSquaresBetweenMoveRange(Token token, List<Square> squaresTocheck, Square lastSquare) {
        return token.isAtBase() || checkThereAreAWallInMoveRange(squaresTocheck) || token.isWinner() || lastSquare.getTokensInside().size() == 2 || token.movedFinalSteps() != 0;
    }

    //hacer generico para distintos tamaños los homepositions.

    private List<Integer> homePositions() {
        return Arrays.asList(5, 22, 39, 55);
    }

    private List<Integer> specialPositions() {
        return Arrays.asList(12, 17, 29, 34, 46, 51, 63, 68);
    }

    private void createSquares(int position) {
        assignSquareWithCurrentType(position);
    }

    private void assignSquareWithCurrentType(int position) {
        if (specialPositions().contains(position)) {
            this.boardSquares.add(new Square(Type.SPECIAL, position));
            return;
        }
        if (homePositions().contains(position)) {
            this.boardSquares.add(new Square(Type.HOME, position));
            return;
        } else this.boardSquares.add(new Square(Type.NORMAL, position));
    }

    //TODO refactor de esto

    public Square getSquareByPosition(int position) {
        position -= 1;
        if (position != -1) {
            return boardSquares.get(position);
        }
        return boardSquares.get(0);
    }

    public void printSquares() {
        boardSquares.forEach(this::printIndex);
        index = 1;
    }

    public void printCurrentStatus(){
        boardSquares.forEach(this::currentTokenInfo);
    }

    private void currentTokenInfo(Square square){
        if (square.getTokensInside().size() > 0){
//            square.getTokensInside().forEach(x -> //logger.info("Hay un token en casilla "+x.retrievePosition()+" del jugador "+x.playerId()));
        }
    }

    public List<Square> retrieveSquaresByIndex(List<Integer> squaresIndexToCheck) {
        return squaresIndexToCheck.stream().map(this::getSquareByPosition).collect(Collectors.toList());
    }

    //TODO => esta logica no se si debe ir aqui
    public void removeTokenFromOriginalSquare(Token movableToken) {
        int originPosition = movableToken.retrievePosition();
        Square originSquare = getSquareByPosition(originPosition);
        moveTokenOutsideSquare(originSquare, movableToken);
    }

    //TODO => esta logica no se si debe ir aqui
    public void addTokenToDestinationSquareAndMove(Token movableToken, int thrown) {
        int destinationSquareIndex = calculateDestinationMove(movableToken.retrievePosition(), thrown);
        Square destinationSquare = getSquareByPosition(destinationSquareIndex);
        moveTokenInsideSquare(destinationSquare, movableToken, thrown);
    }

    //TODO => esta logica no se si debe ir aqui
    private void moveTokenOutsideSquare(Square squareToMoveOutside, Token tokenToRemove) {
        squareToMoveOutside.removeToken(tokenToRemove);
    }

    //TODO => esta logica no se si debe ir aqui
    private void moveTokenInsideSquare(Square squareToMoveInside, Token tokenToAdd, int thrown) {
        squareToMoveInside.addToken(tokenToAdd);
        tokenToAdd.moveTokenInNormalSteps(thrown);
    }

    //TODO => esta logica no se si debe ir aqui
    private void printIndex(Square sq) {
        //logger.info(sq.getType() + " : " + index);
        index++;
    }

    public int calculateDestinationMove(int current, int steps) {
        return ((steps + current) % 68);
    }

}
