package com.adevgar.ddd.parchis.domain.board.squares;

import com.adevgar.ddd.parchis.Game;
import com.adevgar.ddd.parchis.domain.token.PlayerTokens;
import com.adevgar.ddd.parchis.domain.token.Token;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LastPositionsSquares {

    final static Logger logger = Logger.getLogger(String.valueOf(LastPositionsSquares.class));
    private final List<List<Square>> lastPositionsByPlayer = new ArrayList<>();


    public LastPositionsSquares(int numPlayers) {
        createLastPositions(numPlayers);
    }

    //sacar a funciones, que el publico tenga lenguaje semantico y lo privado haga el resto.
    public void addTokenToDestinationSquareAndMove(Token movableToken, int thrown, int playerId) {
        if (haveBeenInsideAlready(movableToken)) {
            int destinationSquareIndex = calculateDestinationMoveComingFromLasPositionSteps(movableToken, movableToken.movedFinalSteps(), thrown);
            Square destinationSquare = getSquareByPosition(destinationSquareIndex, playerId);
            int movement = getRealMovedSteps(movableToken, destinationSquareIndex);
            moveTokenInsideSquare(destinationSquare, movableToken, movement);
        } else {
            int destinationSquareIndex = calculateDestinationMoveComingFromLasPositionSteps(movableToken, 0, thrown);
            Square destinationSquare = getSquareByPosition(destinationSquareIndex, playerId);
            moveTokenInsideSquare(destinationSquare, movableToken, destinationSquareIndex);
        }
    }

    public void removeTokenFromOriginalSquare(Token movableToken, int playerId) {
        int originPosition = movableToken.retrievePositionInLastPositions();
        Square originSquare = getSquareByPosition(originPosition, playerId);
        moveTokenOutsideSquare(originSquare, movableToken);
    }

    public Token getRandomMovableToken(int thrown, PlayerTokens playerTokens) {
        return playerTokens.getTokens()
                .stream()
                .filter(token -> !checkObstacles(token, thrown))
                .findFirst()
                .orElse(null);
    }

    private boolean checkObstacles(Token token, int thrown) {
        if(isInsideLastPositions(token)) {
            return destinationSquareIsFull(token, thrown) || token.isWinner();
        }
        return true;
    }

    private boolean destinationSquareIsFull(Token token, int thrown) {
        int possibleDestination = finalDestionationMove(token, token.movedFinalSteps(), thrown);
        //todo casca aquí, el getSquareByPosition, solo se debería hacer con bounce move.
        Square destinationSquare = getSquareByPosition(possibleDestination, token.playerId());
        return destinationSquare.getTokensInside().size() == 2;
    }

    private boolean isInsideLastPositions(Token token) {
        return token.movedFinalSteps() != 0;
    }


    private void createLastPositions(int numPlayers) {
        IntStream.range(0, numPlayers).forEach(x -> lastPositionsByPlayer.add(createLastSquares()));
    }
    private List<Square> createLastSquares() {
        return IntStream.range(0, 8).mapToObj(this::createLastSquares).collect(Collectors.toList());
    }

    //TODO -> este position +1 ...
    private Square createLastSquares(int pos) {
        return new Square(Type.SPECIAL, pos + 1);
    }

    //todo -> limitar busqueda y tire excepcion , el rango es 1 - 8
    public Square getSquareByPosition(int position, int playerId) {
        position -= 1;
        if (position != -1) {
            return lastPositionsByPlayer.get(playerId).get(position);
        }
        return lastPositionsByPlayer.get(playerId).get(0);
    }


    private void moveTokenOutsideSquare(Square squareToMoveOutside, Token tokenToRemove) {
        squareToMoveOutside.removeToken(tokenToRemove);
    }

    private int getRealMovedSteps(Token movableToken, int destinationIndex) {
        int currentPosition = movableToken.retrievePositionInLastPositions();
        if (destinationIndex >= currentPosition) {
            return destinationIndex - currentPosition;
        }
        return destinationIndex * -1;
    }
    //todo hacer private cuando quitemos la comprobacion de LastMovements
    public int calculateDestinationMoveComingFromLasPositionSteps(Token movableToken, int movedFinalSteps, int thrown) {
        return finalDestionationMove(movableToken, movedFinalSteps, thrown);
    }


    //TODO SACAR CONSTANTES Y FORMULAS
    private int finalDestionationMove(Token movableToken, int movedFinalSteps, int thrown) {
        int movement = movedFinalSteps + thrown;
        int distanceToEnd = (8 - movableToken.retrievePositionInLastPositions());

        if(isDoingMoreThan1Bounce(movedFinalSteps, thrown)){
            int finalMove = -distanceToEnd + (thrown - 8) + 2;
            return finalMove;
        }

        //TODO refactor de esta fórmula
        if (isDoing1Bounce(movement)){
            return 8 - (Math.abs(8-movement));
        }

        else return movement;
    }

    private boolean isDoing1Bounce(int movement) {
        return movement > 8;
    }

    private boolean isDoingMoreThan1Bounce(int movedFinalSteps, int thrown) {
        return thrown - ( 8 - movedFinalSteps) >= 8;
    }
    public boolean haveBeenInsideAlready(Token movableToken) {
        return (movableToken.retrievePositionInLastPositions() > 0);
    }

    private void moveTokenInsideSquare(Square squareToMoveInside, Token tokenToAdd, int destinationSquareIndex) {
        squareToMoveInside.addToken(tokenToAdd);
        tokenToAdd.moveTokenInLastPositionSteps(destinationSquareIndex);
    }

    public void printCurrentStatus(){
        lastPositionsByPlayer.forEach(x-> x.forEach(this::currentTokenInfo));
    }

    private void currentTokenInfo(Square square){
        if (square.getTokensInside().size() > 0){
//            square.getTokensInside().forEach(x -> //logger.info("Hay un token en casilla "+x.retrievePosition()+" del jugador "+x.playerId()));
        }
    }


}
