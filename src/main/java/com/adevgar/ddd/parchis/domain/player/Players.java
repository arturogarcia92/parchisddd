package com.adevgar.ddd.parchis.domain.player;

import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import com.adevgar.ddd.parchis.domain.token.PlayerTokens;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class Players {

    private final List<Player> players = new ArrayList<>();
    final static Logger logger = Logger.getLogger(String.valueOf(Players.class));

    public Players(int playerNumber) {
        createNPlayers(playerNumber);
    }


    public void printPlayersInfo() {
        //logger.info(players.size() + " players in the game");
    }

    public Player getPlayerById(int id) {
        return players.get(id);
    }

    public List<Player> players() {
        return this.players;
    }

    public Player checkWhichPlayerHasToPlay(int currentTurn) {
        return getPlayerById(currentTurn % 4);
    }

    private void createNPlayers(int number) {
        IntStream.range(0, number).forEach(currentPlayerNumber -> players.add(new Player(new PlayerId(currentPlayerNumber), new PlayerTokens(new PlayerId(currentPlayerNumber)))));
        //logger.info("Creating players number: " + number);
    }


}
