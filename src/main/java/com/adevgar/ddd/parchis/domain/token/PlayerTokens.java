package com.adevgar.ddd.parchis.domain.token;

import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import com.adevgar.ddd.parchis.domain.player.PlayerId;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class PlayerTokens {

    private static final int TOKENS_BY_PLAYER = 4;
    private final List<Token> playerTokens = new ArrayList<>();
    private final PlayerId playerId;
    final static Logger logger = Logger.getLogger(String.valueOf(PlayerTokens.class));

    public PlayerTokens(PlayerId playerId) {
        this.playerId = playerId;
        createPlayerTokens();
    }

    public void createPlayerTokens(){
        IntStream.range(0, TOKENS_BY_PLAYER).forEach(x -> this.playerTokens.add(new Token(playerId)));
    }



    public boolean isPlayerWinner(){
        if (playerTokens.stream().allMatch(Token::isWinner)){
            return true;
        }
        return false;
    }


    public List<Token> getTokens() { return this.playerTokens; }

    public boolean areLeftTokensAtBase(){
        return this.playerTokens.stream().anyMatch(Token::isAtBase);
    }

    public void tokenPositions(){
//        //logger.info("Player token positions [ | ");
//        playerTokens.forEach(x -> printPosition(x));
//        //logger.info(" ]");
//        //logger.info("");
    }

    public void printPosition(Token token){
//        if (token.retrievePositionInLastPositions() > 0){
//            //logger.info(token.retrievePositionInLastPositions()+"* | ");
//        }
//        else //logger.info(token.retrievePosition()+" | ");
    }


}
