package com.adevgar.ddd.parchis.domain.token;


import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import com.adevgar.ddd.parchis.domain.player.PlayerId;

import java.util.logging.Logger;

public class Token {


    private static final int BASE_POSITION = -1; // TODO cambiar por boolean
    private int currentPositionInNormalPositionBoard;
    private int currentPositionInLastPositionsBoard;
    private boolean winner;
    private final PlayerId playerId;
    private static final int DISTANCE_BETWEEN_PLAYERS_HOME = 17; // TODO creo que va en tablero
    private static final int FIRST_PLAYER_HOME = 5;// TODO tablero
    private int movedSteps = 0;
    private int movedFinalSteps = 0;
    final static Logger logger = Logger.getLogger(String.valueOf(Token.class));


    //Generar el id solo
    public Token(PlayerId playerId) {
        this.currentPositionInNormalPositionBoard = BASE_POSITION;
        this.playerId = playerId;
    }


    public void spawnToken() {
        //logger.info("The token has been created at position: " + playerHomePosition());
        this.currentPositionInNormalPositionBoard = playerHomePosition();
    }

    public Token die() {
        this.currentPositionInNormalPositionBoard = BASE_POSITION;
        movedSteps = 0;
        return this;
    }

    public int playerHomePosition() {
        int initialPositionOfToken = DISTANCE_BETWEEN_PLAYERS_HOME * playerId.getValue() + FIRST_PLAYER_HOME;
        return (initialPositionOfToken);
    }

    public int retrievePosition() {
        return currentPositionInNormalPositionBoard;
    }

    public int retrievePositionInLastPositions() {
        return currentPositionInLastPositionsBoard;
    }

    public int playerId() {
        return playerId.getValue();
    }

    public boolean isAtBase() {
        return currentPositionInNormalPositionBoard == BASE_POSITION;
    }

    public boolean win() {
        if (movedFinalSteps == 8) {
            //logger.info("Token in goal position: " + (playerId.getValue()+1));
            winner = true;
            return true;
        }
        return false;
    }

    public int moveTokenInNormalSteps(int steps) {

        increaseMovedSteps(steps);
        //logger.info("I moved In Normal Steps " + steps + " steps, from : " + (currentPositionInNormalPositionBoard));
        this.currentPositionInNormalPositionBoard = calculateDestinationMove(currentPositionInNormalPositionBoard, steps);
        //logger.info(" to " + currentPositionInNormalPositionBoard);
        return currentPositionInNormalPositionBoard;
    }

    public int moveTokenInLastPositionSteps(int steps) {
        //logger.info("I moved In Last Position " + steps + " steps, from : " + (currentPositionInLastPositionsBoard) + " to "+ (currentPositionInLastPositionsBoard+steps) );
        increaseLastPositionMovedSteps(steps);
        this.currentPositionInLastPositionsBoard = calculateDestinationMoveInLastPosition(currentPositionInLastPositionsBoard, steps);
        return currentPositionInNormalPositionBoard;
    }


    public int movedSteps() {
        return movedSteps;
    }

    public int movedFinalSteps() {
        return movedFinalSteps;
    }

    private void increaseMovedSteps(int stepsToMove) {
        movedSteps += stepsToMove;
    }

    private void increaseLastPositionMovedSteps(int stepsToMove) {
        movedFinalSteps += stepsToMove;
    }

    private int calculateDestinationMove(int current, int steps) {
        return ((steps + current) % 68);
    }

    private int calculateDestinationMoveInLastPosition(int current, int steps) {
        return ((steps + current) % 68);
    }

    public boolean isWinner() {
        return winner;
    }




}
