package com.adevgar.ddd.parchis.domain.board.dice;

public class SixSidesRandomizableDice implements Randomizable {

    public SixSidesRandomizableDice() {
    }

    public int random(){
     return (int)(Math.random() * 6) + 1;
    }
}
