package com.adevgar.ddd.parchis.domain.board.dice;

public class FixedValueDice implements Randomizable {

    private final int value;

    public FixedValueDice(int value) {
        this.value = value;
    }

    public int random(){
        return value;
    }
}
