package com.adevgar.ddd.parchis.domain.board.dice;

public interface Randomizable {

    int random();

}
