package com.adevgar.ddd.parchis.domain.board.squares;

public enum Type {
    NORMAL,
    HOME,
    SPECIAL
}
