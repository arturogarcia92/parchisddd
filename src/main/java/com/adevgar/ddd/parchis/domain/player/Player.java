package com.adevgar.ddd.parchis.domain.player;

import com.adevgar.ddd.parchis.domain.token.PlayerTokens;

public class Player {

    private final PlayerTokens playerTokens;
    private final PlayerId playerId;


    public Player(PlayerId playerId, PlayerTokens playerTokens) {
        this.playerId = playerId;
        this.playerTokens = playerTokens;
    }

    public PlayerTokens playerTokens(){
        return playerTokens;
    }

    public int id(){ return playerId.getValue(); }


}
