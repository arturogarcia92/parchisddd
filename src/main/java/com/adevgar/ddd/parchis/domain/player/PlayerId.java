package com.adevgar.ddd.parchis.domain.player;

public class PlayerId {


    private final int id;

    public PlayerId(int value) {
        this.id = validate(value);
    }

    private int validate(int value){
        if (value > 4) throw new IllegalStateException("Just 4 players can be in the game");
        return value;
    }

    public int getValue(){
        return id;
    }
}
