package com.adevgar.ddd.parchis.domain.movements;

import com.adevgar.ddd.parchis.domain.board.squares.BoardSquares;
import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import com.adevgar.ddd.parchis.domain.board.squares.Square;
import com.adevgar.ddd.parchis.domain.player.Player;
import com.adevgar.ddd.parchis.domain.token.Token;

public class LastMovements implements Movements {

    private final BoardSquares boardSquares;
    private final Token movableToken;

    public LastMovements(BoardSquares boardSquares, Token movableToken) {
        this.boardSquares = boardSquares;
        this.movableToken = movableToken;
    }

    @Override
    public void doMove(Player player, int thrown) {
        moveInLastPositions(player, thrown);
    }

    private void moveInLastPositions(Player player, int thrown) {

        LastPositionsSquares lastPositionsSquares = boardSquares.retrieveLastPositions();
        removeTokenFromCurrentSquare(player, lastPositionsSquares, movableToken);

        //redo.
        int move = thrown - (63 - movableToken.movedSteps());

        if (lastPositionsSquares.haveBeenInsideAlready(movableToken)) {
            move = thrown;
        }


        lastPositionsSquares.addTokenToDestinationSquareAndMove(movableToken, move, player.id());
        checkIfTokenWon(player, lastPositionsSquares);

    }

    private void checkIfTokenWon(Player player, LastPositionsSquares lastPositionsSquares) {
        if (isTokenAtWinnerPosition()) {
            removeTokenFromGame(player, lastPositionsSquares);
            movableToken.win();
        }
    }

    private boolean isTokenAtWinnerPosition() {
        return movableToken.retrievePositionInLastPositions() == 8;
    }

    private void removeTokenFromGame(Player player, LastPositionsSquares lastPositionsSquares) {
        int currentTokenPosition = movableToken.retrievePositionInLastPositions();
        lastPositionsSquares.getSquareByPosition(currentTokenPosition, movableToken.playerId());
        removeTokenFromCurrentSquare(player, lastPositionsSquares, movableToken);
    }


    private void removeTokenFromCurrentSquare(Player player, LastPositionsSquares lastPositionsSquares, Token movableToken) {
        if (lastPositionsSquares.haveBeenInsideAlready(movableToken)) {
            lastPositionsSquares.removeTokenFromOriginalSquare(movableToken, player.id());
        } else {
            tokenWillBeRemovedByBoardsquares(movableToken);
        }
    }

    private void tokenWillBeRemovedByBoardsquares(Token movableToken) {
        boardSquares.removeTokenFromOriginalSquare(movableToken);
    }


}

