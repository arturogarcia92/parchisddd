package com.adevgar.ddd.parchis;

import com.adevgar.ddd.parchis.domain.board.dice.Dice;
import com.adevgar.ddd.parchis.domain.board.squares.LastPositionsSquares;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class ParchisApplication {

    final static Logger logger = Logger.getLogger(String.valueOf(ParchisApplication.class));

    public static void main(String[] args) {


        //logger.info("Bienvenido al juego del parchis con DDD");
        new Game(4, new Dice()).playGame();





    }


}
